// An action will receive the store as the first argument.
// Since we are only interested in the dispatch (and optionally the state)
// we can pull those two parameters using the ES6 destructuring feature
import Vue from 'vue'
// export const data = function ({ dispatch, state }) {
//   this.$http({url: 'http://api.sellsuki.com', method: 'GET'}).then(
//     function (response) {
//       this.temp = response.data.results[0]
//       console.log(response.data.results[0])
//     }, function (response) { })
//   dispatch('DATA', this.temp)
// }
var MainURL = 'http://staging-api.sellsuki.com/'
var billId = 'O0916I9LFGB00012'
var prepareDate = '2016-09-01 10:56:15'
var Authorization = 'Bearer $GqZIGQE2LbkrsjCs69o48atkGhKxxAW3kQ1aT8ul'
var TrackingNumber = 'TestAdvanceProcess'
var threadId = 't_mid.1467882153717:f0d49ad199c2a43b76'
var accessToken = 'EAAFH0cEkPqkBAGYHdCGJN2yVzCuZAzMoC26G2VgZA1DXo3BTtmJgMxvCfpqZAOjCRhSvJwpx51MpvJzDxVq6mj8OSmpht1bjOIgUx4IN8hgZCjzpBUNpwTUnZCnm3rKZB6KiFE8TNQFep4KxYtqBXerT0LcbSG7ScZD'
var facebookAPIURL = 'https://graph.facebook.com/v2.4/' + threadId + '/messages?access_token=' + accessToken
var TextwithTrack = window.localStorage.Shipedtextwithtracking

export const setBillData = function ({ dispatch, state }, billData) {
  window.localStorage.setItem('results_bill_id', billData.bill.id)
  window.localStorage.setItem('ordering_flow_type', billData.store.ordering_flow_type)
  dispatch('SET_BILL_DATA', billData)
}

export const CallExpressBill = function ({dispatch, state}, ExpressResult) {
  this.$http.put(MainURL + 'bill/billexpress', {bill_id: billId, prepare_date: prepareDate}, {
    headers: {
      'Authorization': Authorization
    }
  }).then((response) => {
    dispatch('SET_EXPRESS_BILL', response.body)
  }, (response) => {
    dispatch('SET_EXPRESS_BILL', response.body)
  })
}

export const setInvoiceBill = function ({dispatch, state}) {
  Vue.$http.get('http://api.sellsuki.com/invoice?bill_url=53Va6A55').then((response) => {
    dispatch('SET_EXPRESS_BILL', response.body)
  }, (response) => {
    dispatch('SET_EXPRESS_BILL', response.body)
  })
}

export const Callnotifyshippment = function ({dispatch, state}) {
  this.$http.put(MainURL + 'bill/billnotifyshipment', {bill_id: billId, ref_code: TrackingNumber}, {
    headers: {
      'Authorization': Authorization
    }
  }).then((response) => {
    dispatch('SET_NOTIFY_SHIPMENT', response.body)
  }, (response) => {
    dispatch('SET_NOTIFY_SHIPMENT', response.body)
  })
}

export const CallInit = function ({dispatch, state}) {
  this.$http.get(MainURL + 'init', {
    headers: {
      'Authorization': Authorization
    }
  }).then((response) => {
    dispatch('SET_INIT_LOGIN', response.body)
  }, (response) => {
    dispatch('SET_INIT_LOGIN', response.body)
  })
}

export const CallBilldeductstock = function ({dispatch, state}) {
  this.$http.put(MainURL + 'bill/billdeductstock', {bill_id: billId}, {
    headers: {
      'Authorization': Authorization
    }
  }).then((response) => {
    dispatch('SET_BILL_DEDUCTSTOCK', response.status)
  }, (response) => {
    dispatch('SET_BILL_DEDUCTSTOCK', response.body)
  })
}

export const SendMessagetocustomer = function ({dispatch, state}) {
  let bodyAPI = 'access_token=' + accessToken + '&' + 'message=' + TextwithTrack + ' ' + TrackingNumber + ' '
  console.log(bodyAPI)
  this.$http.post(facebookAPIURL, {bodyAPI}, {
    headers: {
      'Content-Type': 'text'
    }
  }, {emulateJSON: true, emulateHTTP: true}).then((response) => {
    dispatch('SET_MSG_STATUS', response.body)
  }, (response) => {
    dispatch('SET_MSG_STATUS', response.body)
  })
}

export const CallBillshipment = function ({dispatch, state}) {
  this.$http.put(MainURL + 'bill/billshipment', {bill_id: billId}, {
    headers: {
      'Authorization': Authorization
    }
  }).then((response) => {
    dispatch('SET_BILL_SHIPMENT', response.body)
  }, (response) => {
    dispatch('SET_BILL_SHIPMENT', response.body)
  })
}

export const Expressjob = function ({dispatch, state}) {

}
