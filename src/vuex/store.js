import Vue from 'vue'
import Vuex from 'vuex'

// Make vue aware of Vuex
Vue.use(Vuex)

// Create an object to hold the initial state when
// the app starts up
const state = {
  // When the app starts, count is set to 0
  billData: null,
  expressResult: null,
  notifyShipmentResult: null,
  initInformation: null,
  billDeductstock: null,
  MSGstatus: null,
  billShipment: null
// }
}
// Create an object storing various mutations. We will write the mutation
const mutations = {
  // A mutation receives the current state as the first argument
  // You can make any modifications you want inside this function
  SET_BILL_DATA (state, billData) {
    state.billData = billData
  },
  SET_EXPRESS_BILL (state, expressResult) {
    state.expressResult = expressResult
  },
  SET_NOTIFY_SHIPMENT (state, notifyShipmentResult) {
    state.notifyShipmentResult = notifyShipmentResult
  },
  SET_INIT_LOGIN (state, initInformation) {
    state.initInformation = initInformation
  },
  SET_BILL_DEDUCTSTOCK (state, billDeductstock) {
    state.billDeductstock = billDeductstock
  },
  SET_MSG_STATUS (state, MSGstatus) {
    state.MSGstatus = MSGstatus
  },
  SET_BILL_SHIPMENT (state, billShipment) {
    state.billShipment = billShipment
  }
}

// Combine the initial state and the mutations to create a Vuex store.
// This store can be linked to our app.
export default new Vuex.Store({
  state,
  mutations})
