// This getter is a function which just returns the count
// With ES6 you can also write it as:

export const getBillData = state => state.billData
export const getExpressBillResponse = state => state.expressResult
export const getInvoiceBill = state => state.expressResult
export const getNotifyshippment = state => state.notifyShipmentResult
export const getInit = state => state.initInformation
export const getBilldeductstock = state => state.billDeductstock
export const getMSGstatus = state => state.MSGstatus
export const getBillshipment = state => state.billShipment
