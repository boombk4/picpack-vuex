import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import Scanbill from './components/Scanbill'
import Scanproduct from './components/Scanproduct'
import login from './components/login'
import preparelogin from './components/preparelogin'
import modalappend from './components/modalappend'
import VueResource from 'vue-resource'
import modalBeforeout from './components/modalBeforeout'
import picturemodal from './components/picturemodal'
// import {getEnv} from './service/environment/'

// var VueFire = require('vuefire')

// Vue.use(VueFire)
Vue.use(VueResource)
Vue.use(VueRouter)

var Not = Vue.extend({
  template: 'Error: Invalid link'
})
var router = new VueRouter({
  hashbang: false
})

router.map({
  '*': {
    component: Not
  },
  '/': {
    component: Scanbill
  },
  '/Scanproduct': {
    component: Scanproduct
  },
  '/ScanBill': {
    component: Scanbill
  },
  '/login': {
    component: login
  },
  '/prepare_login/:storeId': {
    component: preparelogin
  },
  '/modalappend': {
    component: modalappend
  },
  '/modalBeforeout': {
    component: modalBeforeout
  },
  '/picturemodal': {
    component: picturemodal
  }
})

// router.redirect({
//   '*': '/'
// })

// router.beforeEach(function (transition) {
//   if (document.cookie.search('sellsuki.fblogintoken') !== -1 && document.cookie.search('sellsuki.facebook') !== -1 && document.cookie.search('sellsuki.user') !== -1
// /* &&document.cookie.search('sellsuki.store_' + this.$route.params.storeId) !== -1*/) {
//     transition.next()
//   } else {
//     // window.location.href = getEnv().loginPortal
//     transition.next()
//   }
// })

router.start(App, 'app')
/* eslint-disable no-new */
// new Vue({
//   el: 'body',
//   components: { App }
// })
