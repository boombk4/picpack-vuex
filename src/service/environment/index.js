export function getEnv () {
  if (window.location.origin === 'http://localhost:8080') {
    return {
      sellsukiAPI: 'http://192.168.100.127:8003/',
      loginPortal: 'http://localhost:3000/'
    }
  } else {
    return {
      sellsukiAPI: 'https://api.sellsuki.com',
      loginPortal: 'http://login.sellsuki.com'
    }
  }
}
